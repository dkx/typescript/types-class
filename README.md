# DKX/TypesClass

Typescript definition for class

## Installation

```bash
$ npm install --save-dev @dkx/types-class
```

## Usage

```typescript
import {ClassType} from '@dkx/types-class';

class TestClass {}
function acceptClass(classType: ClassType<TestClass>): void {}

acceptClass(TestClass);
```
