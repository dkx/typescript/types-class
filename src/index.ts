export interface ClassType<T> extends Function
{


	new(...args: Array<any>): T;

}
